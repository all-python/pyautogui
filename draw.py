import pyautogui
import subprocess
import time
subprocess.Popen("mspaint.exe")
time.sleep(5)
screenWidth, screenHeight = pyautogui.size()  # Get the size of the primary monitor.
currentMouseX, currentMouseY = pyautogui.position()
pyautogui.moveTo(300, 300)
distance = 200
while distance > 0:
        pyautogui.drag(distance, 0, duration=0.1)   # move right
        distance -= 5
        pyautogui.drag(0, distance, duration=0.1)   # move down
        pyautogui.drag(-distance, 0, duration=0.1)  # move left
        distance -= 5
        pyautogui.drag(0, -distance, duration=0.1)  # move up

